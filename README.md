# CareWeb Web Apps
Web applications configured to run CareWeb Framework.

## Using the Web App Template

The Web App Template is a Maven archetype for constructing
a custom web application.  To use it from the command line,
move to the folder that is to serve as the parent folder of
the project and enter the following:

`mvn archetype:generate -DarchetypeGroupId=org.hspconsortium.carewebframework -DarchetypeArtifactId=cwf-webapp-archetype`

and follow the prompts.  